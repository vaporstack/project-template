import shutil
import os
import subprocess


iff = "src/template-0.0.1-annotated"

off = iff.replace("annotated", "clean")
off = off.replace("src", "dist")

print off

try:
	shutil.rmtree(off)
except:
	pass


shutil.copytree(iff, off)

pwd = os.getcwd()
os.chdir(off)


# subprocess.check_output('touch asdf.txt', shell=True)

cmd = "find . -iname 'notes.txt' | xargs rm"
print subprocess.check_output(cmd, shell=True)

os.chdir(pwd)
